# README #

### This is some R code to analyze data for Microsoft and Apple stock values. ###

In order to demonstrate one way to think about stock analyses, we are wondering if the percent change in daily close price for these two stocks are correlated.

Don't forget that choosing "Source" on the sidebar is the way to look at the finished document. 
Choose the .md file.

* Aaron Gowins
* jagowins@gmail.com

nyTimes.md is an api scrub of nytimes to find keyword "microsoft"

apl.md is an api scrub of nytimes to find keyword "Apple"
