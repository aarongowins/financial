# Analyzing stock values with R and Python
aaron gowins  
August 12, 2015  

## 

We will load some data from the popular [yahoo finance](http://finance.yahoo.com) site. I have been using a microsoft desktop on my mac for a windows platform I use at the NIH, so it popped into my head to compare Microsoft and Apple stock prices.


```
## [1] "MSFT"
```

```
## [1] "AAPL"
```

![](Analysis_files/figure-html/unnamed-chunk-1-1.png) ![](Analysis_files/figure-html/unnamed-chunk-1-2.png) 

Obviously something is going on with the Apple graph sometime in 2014. 

During a quick search we learn that Apple stock indeed was split on [June 9, 2014 ](http://money.cnn.com/2014/06/09/investing/apple-stock-split-reactions/).

R's quantmod package makes it easy to load data and perform some basic analyses, but it's a little cumbersome to do custom data manipulations, so let's switch gears here and go back to a regular data frame to rewrite the Apple data as if it were split all along.



```
## [1] "MSFT"
```

```
## [1] "AAPL"
```

![](Analysis_files/figure-html/unnamed-chunk-2-1.png) 

This comparison makes much more sense when we imagine that Apple stock had been split all along, of course we could normalize the stocks on initial value or something, but this is a good look at the trajectories. 

Looking at the data makes me wonder if the values of these two stocks are correlated. We will find the differences between closing prices for each day and see if there is an association.


```
## 
## 	Pearson's product-moment correlation
## 
## data:  AAPL$percent_change and MSFT$percent_change
## t = 52.1178, df = 2187, p-value < 2.2e-16
## alternative hypothesis: true correlation is not equal to 0
## 95 percent confidence interval:
##  0.7250043 0.7624140
## sample estimates:
##       cor 
## 0.7442924
```

Not only are the prices correlated, but highly so. Being more of a science guy I can't say much about the correlation between stocks in general. I imagine there is some correlation between many stocks, since many days seem to be generally up or down days. Surely there are also negative correlations like the price of gas and sale of pickups.

This could be a discussion of its own, and I imaging we could spend many weeks delving into the ways correlations work in stock price data. We will return to R when we want to compare some models, but I was anxious to see what Python had to offer, and decided to switch gears. Although R has very sophisticated statistical tools and solid, simple markdown tools like this one, Python has Matplotlib, with it's amazing graphics and some powerful, incredibly fast tools for financial data analysis. 

Although R has excellent online support that makes it really easy to learn a particular task, Python has [pythonprogramming.net](http://pythonprogramming.net). It's usually easier to figure out how to wrangle data in R, but the speed of Python makes it worthwhile to trudge through the sometimes clumsy documentation. We will see the power of R's custom analytics later, but with a little research and work we can produce something in Python that is about as slick as you like.

In the following plots we have the top section devoted to the RSI, or "Relative Strength Index" for each stock. $RSI=100-\frac{100}{1+RS}$ where $RS=\frac{\text{average of n days' up closes}}{\text{average of n days' down closes}}$ The RSI is a momentum indicator, designed to capture the condition of being over or undersold. A value above 70 suggests that the asset may be overvalued, while a score under 30 suggests that the asset may be oversold and therefore undervalued.

The bottom section is a plot of the MACD, or Moving Average Convergence Divergence. It's calculated by subtracting the 26-day exponential moving average (EMA) from the 12-day (EMA). It is plotted along with the 9-day EMA of the MACD, which is known as the signal line. There are three key things to watch for; crossovers, divergence, and dramatic changes. There is quite a bit to say about how this works, there's a great tutorial [here](http://stockcharts.com/school/doku.php?id=chart_school:technical_indicators:moving_averages).

The main plots have a stock price and volume time series. The stock prices are plotted along with simple moving averages, which is a secant line derivative approximation for 10 and 50 day time intervals for the stock price. 

Sorry, for now you will have to open the Python plots from "source" on the sidebar in bitbucket.
They are called "MicrosoftPYplot.png", and "ApplePYplot.png". They do look nice that way...



![alt text](/Users/gowinsja/Documents/financial/MicrosoftPYplot.png)



![alt text](/Users/gowinsja/Documents/financial/ApplePYplot.png)
